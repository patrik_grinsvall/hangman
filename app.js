'use strict';
// Load the TCP Library
var net = require('net');

// Keep track of the chat clients
var clients = [];

var words = ['rorschach', 'bookworm', 'buckaroo', 'oxygen', 'pixel', 'uptown', 'cognac', 'kamikaze', 'karaoke', 'ukulele', 'abyss', 'genuine', 'excrement'];
var word;
var guessedWord = "";
var tries = 0;
var tmp;
var gameover;

/**
 * Resets the name
 */
function init() {
	var x, max = words.length, min = 0, randomnumber = Math.floor(Math.random() * (max - min + 1)) + min;
	word = words[randomnumber];
	guessedWord = "";
	for (x = 1; x <= word.length; x++) {
		guessedWord += "*";
	}
	tries = 0;
	gameover = false;
}

/**
 * Return the ascii hangman
 */
function getTries() {
	var gfx = [
		'+------------------+\r\n|                  |\r\n|                  |\r\n|                  |\r\n|                  |\r\n|                  |\r\n|                  |\r\n|                  |\r\n+------------------+\r\n',
		'+------------------+\r\n|                  |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n+------------------+\r\n',
		'+------------------+\r\n|       _______    |\r\n|      |/          |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n+------------------+\r\n',
		'+------------------+\r\n|       _______    |\r\n|      |/      |   |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n+------------------+\r\n',
		'+------------------+\r\n|       _______    |\r\n|      |/      |   |\r\n|      |      (_)  |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n|      |           |\r\n+------------------+\r\n',
		'+------------------+\r\n|       _______    |\r\n|      |/      |   |\r\n|      |      (_)  |\r\n|      |       |   |\r\n|      |       |   |\r\n|      |           |\r\n|      |           |\r\n+------------------+\r\n',
		'+------------------+\r\n|       _______    |\r\n|      |/      |   |\r\n|      |      (_)  |\r\n|      |      \\|/  |\r\n|      |       |   |\r\n|      |           |\r\n|      |           |\r\n+------------------+\r\n',
		'+------------------+\r\n|       _______    |\r\n|      |/      |   |\r\n|      |      (_)  |\r\n|      |      \\|/  |\r\n|      |       |   |\r\n|      |      / \\  |\r\n|      |           |\r\n+------------------+\r\n'
	];
	return gfx[tries];
}

/**
 * Replace a character in string at index
 * @param index
 * @param char
 * @param string
 * @returns {string}
 */
function replaceAt(index, char, string) {
	var a = string.split("");
	a[index] = char;
	return a.join("");
}

/**
 * Tests if input word is correct or if any character is correct.
 *
 * @param input
 * @returns {number}
 */
function testWord(input) {
	var i, right = false;
	if (input === word) {

		return 1;
	}
	for (i in word) {
		if (input[0] === word[i]) {
			guessedWord = replaceAt(i, word[i], guessedWord);
			console.log("guessed: ", guessedWord, input[0], "-", word[i], "-", i);
			right = true;
		}
	}
	if (right) return 2;
	tries++;
	return 0;
}

function testCommands(word, socket) {
	switch (word) {
	case "/exit" :
		socket.end();
		clients.splice(clients.indexOf(socket), 1);
		break;
	case "/msg" :
		broadcast(socket.name + ":" + word);
		break;
	}
}

init();

// Start a TCP Server
net.createServer(function (socket) {

	// Identify this client
	socket.name = socket.remoteAddress + ":" + socket.remotePort;

	// Put this new client in the list
	clients.push(socket);

	// Send a nice welcome message and announce
	socket.write("\n================================\nWelcome " + socket.name + "\nGuess for a character by typing it and press enter.\nGuess for the whole word by typing the whole word\n================================\n");
	broadcast(socket.name + " joined the game\n", socket);

	// Handle incoming messages from clients.
	socket.on('data', function (data) {
		var clientWord = data.toString().trim();

		if(testCommands(clientWord, socket)) return;

		switch (testWord(clientWord)) {
		case 1:
			tmp = socket.name + " guessed for WORD  ->" + clientWord + "<- which is RIGHT! " + socket.name + " Won the game!";
			console.log(tmp);
			broadcast(tmp, socket);
			break;
		case 2:
			tmp = getTries() + socket.name + " guessed for ->" + clientWord[0] + "<- which is RIGHT! The correct characters are now: " + guessedWord;
			console.log(tmp);
			broadcast(tmp, socket);
			break;
		case 0:
			if (tries === 8) {
				tmp = getTries() + socket.name + " guessed for ->" + clientWord[0] + "<- which is WRONG! YOU HANGED HIM! GAME OVER! The correct Word was: ->" + word + "<-\nStarting a new game\n";
				init();
			} else {
				tmp = getTries() + socket.name + " guessed for ->" + clientWord[0] + "<- which is WRONG! The guessed characters are now: " + guessedWord;
			}
			broadcast(tmp, socket);
			break;
		}


	});

	// Remove the client from the list when it leaves
	socket.on('end', function () {
		clients.splice(clients.indexOf(socket), 1);
		broadcast(socket.name + " left the game.\n");
	});

	// Send a message to all clients
	function broadcast(message, sender) {
		clients.forEach(function (client) {
			// Don't want to send it to sender
			//if (client === sender) return;
			client.write(message + "\r\n");
		});
		// Log it to the server output too
		process.stdout.write(message);
	}

}).listen(6000);

// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 5000\n");
